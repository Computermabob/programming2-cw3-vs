// StockItem.h : Defines the interface for StockItem.c

#ifndef STOCKITEM
#define STOCKITEM

#include<iostream>
#include<list>
#include<vector>
#include<string>

using namespace std;

class StockItem {
protected:
    
    int unitCount;
    int price; // Price is in pence as per the specification
	string misc;
	string stockCode;

public:
	// Constructor
	
	StockItem(string stockCode, int unitCount, int price, string misc);
	StockItem(string stockCode, int unitCount, int price);
	StockItem();
	StockItem(vector<string> * data);

    string getStockCode() {
        return stockCode;
    }

    int getUnitCount() {
        return unitCount;
    }

    const int getPrice() {
        return price;
    }

	string getMisc() {
		return misc;
	}

    virtual string toString();

    bool operator<(const StockItem& b) const;
	int operator-(const StockItem& b) const;
	static bool compare(const StockItem& a, const StockItem& b);
};

class Resistor : public StockItem {
private:
	double resistance;
public:
	Resistor(vector<string> * data);
	Resistor(string stockCode, int unitCount, int price, string misc);

	double getResistance();
	
	double operator-(Resistor& b);
	double operator+(Resistor& b);
};

class Capacitor : public StockItem {
public:
	Capacitor(vector<string> * data);
	Capacitor(string stockCode, int unitCount, int price, string misc);
};

class Diode : public StockItem {
public:
	Diode(vector<string> * data);
	Diode(string stockCode, int unitCount, int price);
};

class Transistor : public StockItem {
public:
	enum type {NPN, PNP, PET};
private:
	type deviceType;
public:
	
	Transistor(vector<string> * data);
	Transistor(string stockCode, int unitCount, int price, string misc);

	type getType();
	string getTypeSttring();

};

typedef class IntegratedCircuit : public StockItem {
public:
	IntegratedCircuit(vector<string> * data);
	IntegratedCircuit(string stockCode, int unitCount, int price, string misc);
} IC;


#endif // !STOCKITEM
