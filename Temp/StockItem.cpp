// StockItem.cpp : Defines the main part of the solution
//

#include "StockItem.h"

/*
Super class to super any stock items
 */
// Super constructor
StockItem::StockItem(string stockCode, int unitCount, int price, string misc) {
    this->stockCode = stockCode;
    this->unitCount = unitCount;
    this->price = price;
	this->misc = misc;
}

StockItem::StockItem(string stockCode, int unitCount, int price) {
	this->stockCode = stockCode;
	this->unitCount = unitCount;
	this->price = price;
	this->misc = "";
}

StockItem::StockItem() {
    this->stockCode = "null";
    this->unitCount = 0;
    this->price = 0;
	this->misc = "";
}

StockItem::StockItem(vector<string> * data) {
	this->stockCode = data->at(1);
	this->unitCount = stoi(data->at(2));
	this->price = stoi(data->at(3));
	if (data->size() == 5) {
		this->misc = data->at(4);
	}
	else
		this->misc = "";
}

string StockItem::toString() {
    string output;
    output += "\nCode:  " + this->getStockCode() + "\n";
    output += "Price: GBP " + to_string((double)this->getPrice() / 100) + "\n";
    output += "Count: " + to_string(this->getUnitCount()) + "\n";
	if (this->misc != "")
		output += "Misc:  " + this->getMisc() + "\n";
    return output;
}

bool StockItem::operator<(const StockItem& b) const {
    return price < b.price;
}
int StockItem::operator-(const StockItem& b) const {
	return price - b.price;
}

/*
Resistor class to represent a resistor stock type
    - Implements StockItem
 */
// Constructor
Resistor::Resistor(vector<string> * data) : StockItem(data) {
	this->resistance = this->getResistance();
}
Resistor::Resistor(string stockCode, int unitCount, int price, string misc)
	: StockItem(stockCode, unitCount, price, misc) {
	this->resistance = this->getResistance();
}

double Resistor::getResistance() {
	string resistorCode = this->misc;
	double resistance = 0;
	for (int i = 0; i < resistorCode.length(); i++) {
		if (isalpha(resistorCode[i])) {
			if (i == 1) {
				switch (resistorCode[1]) {
				case 'R':
					resistance = ((double)resistorCode[0] - '0') + (((double)resistorCode[2] - '0') / 10.0);
					break;
				case 'K':
					resistance = (((double)resistorCode[0] - '0') * 1000.0) + (((double)resistorCode[2] - '0') * 100.0);
					break;
				case 'M':
					resistance = (((double)resistorCode[0] - '0') * 1000000.0) + (((double)resistorCode[2] - '0') * 100000.0);
					break;
				}
			}
			else {
				switch (resistorCode[i]) {
				case 'R':
					resistance = stod(resistorCode.substr(0, i), nullptr);
					break;
				case 'K':
					resistance = stod(resistorCode.substr(0, i), nullptr) * 1000.0;
					break;
				case 'M':
					resistance = stod(resistorCode.substr(0, i), nullptr) * 1000000.0;
					break;
				}
			}
		}
	}
    return resistance * (double)this->getUnitCount();
}


// Operator overloads
double Resistor::operator+(Resistor& b) {
    return this->resistance + b.resistance;
}

double Resistor::operator-(Resistor& b) {
    return this->resistance - b.resistance;
}

/*
Capacitor class to represent a capacitor stock type
    - Implements StockItem
 */
// Constructor
Capacitor::Capacitor(string stockCode, int unitCount, int price, string misc) : 
	StockItem(stockCode, unitCount, price) {}

Capacitor::Capacitor(vector<string> * data) : 
	StockItem(data) {}



/*
Diode class to represent a diode stock type
    - Implements StockItem
 */
// Constructor
Diode::Diode(string stockCode, int unitCount, int price) :
	StockItem(stockCode, unitCount, price) {}

Diode::Diode(vector<string> * data) : 
	StockItem(data) {}

/*
Transistor class to represent a capacitor stock type
    - Implements StockItem
 */
// Constructor
Transistor::Transistor(string stockCode, int unitCount, int price, string misc) :
	StockItem(stockCode, unitCount, price, misc) {
	this->deviceType = this->getType();
}
Transistor::Transistor(vector<string> * data) :
	StockItem(data) {
	this->deviceType = this->getType();
}
// Getters
Transistor::type Transistor::getType() {
	if (this->misc == "NPN")
		return NPN;
	else if (this->misc == "PNP")
		return PNP;
	else // If all else fails it should be PET
		return PET;
}


/*
Integrated Circuit class to represent an IC stock type
    - Implements StockItem
 */
// Constructor
IntegratedCircuit::IntegratedCircuit(string stockCode, int unitCount, int price, string misc) :
	StockItem(stockCode, unitCount, price, misc) {}
IntegratedCircuit::IntegratedCircuit(vector<string> * data) :
	StockItem(data) {}