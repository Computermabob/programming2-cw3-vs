#include "Inventory.h"

using namespace std;

int main() {
    Inventory* inv = new Inventory("inventory.txt");
    //int finish;

	inv->sortPrice();
	//for (StockItem * item : inv->getStockItemVector()) {
	//	cout << item->getPrice() << endl;
	//}
	cout << inv->toString() << endl;
	
	cout << "Item with the most stock: " << inv->getMostStock()->toString() << endl;
	
	cout << "Number of NPN transistors in stock: " << inv->countTransistors(Transistor::type::NPN) << endl;
    
	cout << "\nTotal Resistance: " << to_string(inv->getTotalResistance()) << endl;

	cout << "\nNumber of stock items with prices above 10p: " << inv->numberAbovePrice(10) 
		<< " out of " << inv->getStockItemVector().size() << endl;

	cout << "\nPress any button to exit . . ." << endl;
	cin.get();
	return 0;
}