// StockItem.cpp : Defines the main part of the solution
//

#include "Inventory.h"
#include "StockItem.h"
#include <iostream>
#include <string>
#include <iterator>
#include <algorithm>

using namespace std;

Inventory::Inventory(StockItem stockItemList[], int size) {
	for (int i = 0; i < size; i++) {
		Inventory::stockItemSet.push_back(&stockItemList[i]);
	}
}

Inventory::Inventory(string fileURL) {
	ifstream readFile(fileURL);
	if (readFile) {

		string line;
		bool done = false;

		while (getline(readFile, line, '\n')) {
			stringstream stream(line);
			vector<string> data;
			while (stream.good()) {
				string substr;
				getline(stream, substr, ',');
				trim(substr);
				data.push_back(substr);
			}

			if(data.at(0) == "resistor") {
				StockItem* item = new Resistor(&data);
				stockItemSet.push_back(item);
			}
			else if (data.at(0) == "capacitor") {
				StockItem* item = new Capacitor(&data);
				stockItemSet.push_back(item);
			}
			else if (data.at(0) == "diode") {
				StockItem* item = new Diode(&data);
				stockItemSet.push_back(item);
			}
			else if (data.at(0) == "transistor") {
				StockItem* item = new Transistor(&data);
				stockItemSet.push_back(item);
			}
			else if (data.at(0) == "IC") {
				StockItem* item = new IC(&data);
				stockItemSet.push_back(item);
			}
		}
	}
}

vector<StockItem*> Inventory::getStockItemVector() {
	return stockItemSet;
}

double Inventory::getTotalResistance() {
	double total = 0;
	for (StockItem* item : stockItemSet) {
		if (dynamic_cast<Resistor*>(item) != nullptr) {
			Resistor * resistor = (Resistor*)item;
			total += resistor->getResistance();
		}
	}
	return total;
}

void Inventory::sortPrice() {
	bool complete = true;
	for (int i = 0; i < stockItemSet.size()-1; i++) {
		if (stockItemSet[i]->getPrice() < stockItemSet[i+1]->getPrice()) {
			swap(stockItemSet[i], stockItemSet[i + 1]);
			complete = false;
		}
	}
	if (!complete) {
		sortPrice();
	}
}

StockItem * Inventory::getMostStock() {
	StockItem * output = new StockItem();
	for (StockItem * item : stockItemSet) {
		if (output->getUnitCount() < item->getUnitCount())
			output = item;
	}
	return output;
}

int Inventory::countTransistors(Transistor::type type) {
	int counter = 0;
	for (StockItem * item : stockItemSet) {
		if (dynamic_cast<Transistor*>(item) != nullptr) {
			Transistor * newItem = (Transistor*)item;
			if (newItem->getType() == type)
				counter+= newItem->getUnitCount();
		}
	}
	return counter;
}

int Inventory::numberAbovePrice(int price) {
	int counter = 0;
	for (StockItem * item : stockItemSet) {
		if (item->getPrice() > 10)
			counter++;
	}
	return counter;
}

string Inventory::toString() {
	string output;
	for (StockItem * item : stockItemSet) {
		output += item->toString();
	}
	return output;
}

void trim(string &str) {
	for (int i = 0; i < str.length(); i++)
		if (str[i] == ' ') {
			str.erase(i, 1);
			i--;
		}
}