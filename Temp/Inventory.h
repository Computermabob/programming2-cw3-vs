// StockItem.h : Defines the interface for StockItem.c

#ifndef INVENTORY
#define INVENTORY

#include <fstream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "StockItem.h"

using namespace std;

class Inventory {
private:
	vector<StockItem*> stockItemSet;
public:
	
    Inventory(StockItem stockItemList[], int size);
    Inventory(string fileURL);
	vector<StockItem*> getStockItemVector();
    double getTotalResistance();

	void sortPrice(); // Sort the list by price of item
	StockItem * getMostStock();
	int countTransistors(Transistor::type type);
	int numberAbovePrice(int price);

	string toString();
};

void trim(string &str);

#endif // !INVENTORY